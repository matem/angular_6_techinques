import {PartOneVerifier} from './verifiers/part-one.verifier';
import {PartTwoVerifier} from './verifiers/part-two.verifier';

export class VerifiersService {
  constructor(
    private verifierPartOne: PartOneVerifier,
    private verifierPartTwo: PartTwoVerifier
  ) {
  }

  verify() {
    const partOneResult = this.verifierPartOne.verifyPartOne();
    const partTwoResult = this.verifierPartTwo.verifyPartTwo();
    return partOneResult && partTwoResult;
  }
}
