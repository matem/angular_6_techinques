import {PartOneVerifier} from './verifiers/part-one.verifier';
import {PartTwoVerifier} from './verifiers/part-two.verifier';
import {Verifier} from './verifiers/verifier.abstract';

export class VerifiersService {
  private verifiers: Verifier[] = [];

  constructor(
    private partOneVerifier: PartOneVerifier,
    private partTwoVerifier: PartTwoVerifier
  ) {
    this.verifiers.push(this.partTwoVerifier, this.partOneVerifier);
  }

  private verify() {
    return this.verifiers.filter(() => {
      /** SOME_PREDICATE */
      return true;
    }).map(((verifier: Verifier): boolean => {
      return verifier.verify();
    })).reduce((prev, curr) => {
      return prev && curr;
    }, true);
  }
}
