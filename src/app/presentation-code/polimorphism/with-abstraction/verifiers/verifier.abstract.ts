export abstract class Verifier {
  type: string;

  public abstract verify(): boolean;
}
