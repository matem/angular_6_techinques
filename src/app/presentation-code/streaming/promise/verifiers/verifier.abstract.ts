export abstract class Verifier {
  type: string;

  public abstract verify(): Promise<boolean>;
}
