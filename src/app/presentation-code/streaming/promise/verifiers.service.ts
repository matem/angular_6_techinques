import {Injectable} from '@angular/core';
import {PartOneVerifier} from './verifiers/part-one.verifier';
import {PartTwoVerifier} from './verifiers/part-two.verifier';

@Injectable()
export class VerifiersService {
  constructor(
    private partOneVerifier: PartOneVerifier,
    private partTwoVerifier: PartTwoVerifier
  ) {
  }

  public verifyCascade() {
    this.partOneVerifier.verify().then((response1) => {
      this.partTwoVerifier.verify().then((response2) => {
      });
    });
  }

  public verifyParallel() {
    const promises = [];
    promises.push(this.partOneVerifier.verify().then((response) => {
      this.processResponse(response);
    }));
    promises.push(this.partTwoVerifier.verify().then((response) => {
      this.processResponse(response);
    }));
    Promise.all(promises).then(() => {
    });
  }

  private processResponse(resp) {
  }
}
