import {Injectable} from '@angular/core';
import {PartOneVerifier} from './verifiers/part-one.verifier';
import {PartTwoVerifier} from './verifiers/part-two.verifier';
import {Verifier} from './verifiers/verifier.abstract';
import {forkJoin, Observable} from 'rxjs';

@Injectable()
export class VerifiersService {
  verifiers: Verifier[] = [];

  constructor(
    private partOneVerifier: PartOneVerifier,
    private partTwoVerifier: PartTwoVerifier,
  ) {
    this.verifiers.push(this.partOneVerifier, this.partTwoVerifier);
  }

  verify() {
    const verifications$: Observable<boolean>[] = [];
    this.verifiers.forEach((verifier: Verifier) => {
      verifications$.push(verifier.verify());
    });
    forkJoin(verifications$).pipe(
    ).subscribe(() => {
    });
  }
}
