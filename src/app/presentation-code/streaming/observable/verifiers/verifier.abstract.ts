import {Observable} from 'rxjs/Observable';

export abstract class Verifier {
  type: string;

  public abstract verify(): Observable<boolean>;
}
