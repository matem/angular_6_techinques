import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {filter, map} from 'rxjs/operators';
import {Observable} from 'rxjs';

export enum Color {
  RED = 'RED',
  GREEN = 'GREEN',
  BLUE = 'BLUE'
}

export class Car {
  maxSpeed: string;
  horsePower: number;
  color: Color;

  constructor(carObject) {
    this.maxSpeed = carObject.maxSpeed;
    this.horsePower = carObject.horsePower;
    this.color = carObject.color;
  }

  isRed() {
    return this.color === Color.RED;
  }
}

@Injectable()
export class CarsService {
  constructor(private http: HttpClient) {
  }

  public getCar(id): Observable<Car> {
    return this.http.get(`/cars/${id}`).pipe(
      map((response: any) => {
        return new Car(response);
      })
    );
  }

  public getCars(): Observable<Car[]> {
    return this.http.get(`/cars`).pipe(
      map((response) => {
        const predicate = response['cars'] && Array.isArray(response['cars']);
        return predicate ? response['cars'] : [];
      }),
      map((carsObjectsArray: Array<any>) => {
        return carsObjectsArray.map((carObject) => {
          return new Car(carObject);
        });
      })
    );
  }

  public getRedCars(): Observable<Car[]> {
    return this.getCars().pipe(
      map((response: any) => {
        return response.filter((car: Car) => {
          return car.isRed();
        });
      })
    );
  }

  public getCarsByColor(color: Color): Observable<Car[]> {
    return this.getCars().pipe(map((cars: Car[]) => {
      return cars.filter((car: Car) => {
        return car.color === color;
      });
    }));
  }
}
