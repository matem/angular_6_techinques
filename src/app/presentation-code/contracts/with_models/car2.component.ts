import {Component, OnInit} from '@angular/core';
import {Car, CarsService} from './cars.service';
import {filter, map} from 'rxjs/operators';

@Component({
  selector: 'app-cars',
  template: ``,
  styles: []
})
export class Car2Component implements OnInit {
  private car: Car = new Car({});

  constructor(private carsService: CarsService) {
  }

  getCarWithModel(id) {
    this.carsService.getCar(id).subscribe((car: Car) => {
      this.car = car;
    });
  }

  get maxSpeed() {
    return this.car.maxSpeed;
  }

  ngOnInit(): void {
    this.getCarWithModel(1);
  }
}
