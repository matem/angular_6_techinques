import {Component} from '@angular/core';
import {Car, CarsService} from './cars.service';

@Component({
  selector: 'app-cars-2',
  template: ``,
  styles: []
})
export class Cars2Component {
  private cars;

  constructor(private carsService: CarsService) {
  }

  private getCars() {
    this.carsService.getCars().subscribe((cars: Car[]) => {
      this.cars = cars;
    });
  }
}
