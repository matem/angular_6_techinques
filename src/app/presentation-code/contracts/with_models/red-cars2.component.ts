import {Component, OnInit} from '@angular/core';
import {CarsService, Color} from './cars.service';

@Component({
  selector: 'app-red-cars-2',
  template: ``,
  styles: []
})
export class RedCars2Component implements OnInit {
  private redCars;

  constructor(private carsService: CarsService) {
  }

  getRedCars() {
    this.carsService.getRedCars().subscribe((redCars) => {
      this.redCars = redCars;
    });
  }

  getRedCars2() {
    this.carsService.getCarsByColor(Color.RED).subscribe((redCars) => {
      this.redCars = redCars;
    });
  }

  ngOnInit(): void {
    this.getRedCars();
  }
}
