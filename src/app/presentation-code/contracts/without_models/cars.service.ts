import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

enum Color {
  RED = 'RED',
  GREEN = 'GREEN',
  BLUE = 'BLUE'
}

export class Car {
  maxSpeed: string;
  horsePower: number;
  color: Color;

  constructor(carObject) {
    this.maxSpeed = carObject.maxSpeed;
    this.horsePower = carObject.horsePower;
    this.color = carObject.color;
  }

  isRed() {
    return this.color === Color.RED;
  }
}

@Injectable()
export class CarsService {
  constructor(private http: HttpClient) {
  }

  public getCarWithoutModel(id): Observable<any> {
    return this.http.get(`/cars/${id}`);
  }

  public getCarsWithoutModel(): Observable<any> {
    return this.http.get(`/cars`);
  }
}
