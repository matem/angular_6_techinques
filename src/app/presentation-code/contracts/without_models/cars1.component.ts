import {Component, OnInit} from '@angular/core';
import {Car, CarsService} from './cars.service';

@Component({
  selector: 'app-cars-1',
  template: ``,
  styles: []
})
export class Cars1Component implements OnInit {
  private cars: Car[];

  constructor(private carsService: CarsService) {
  }

  getCars() {
    this.carsService.getCarsWithoutModel().subscribe((response) => {
      if (response['cars'] && Array.isArray(response['cars'])) {
        let cars = <Array<any>>response['cars'];
        cars = cars.map((carObject) => {
          return new Car(carObject);
        });
        this.cars = cars;
      } else {
        this.cars = [];
      }
    });
  }

  ngOnInit(): void {
    this.getCars();
    console.log(this.cars);
  }
}
