import {Component, OnInit} from '@angular/core';
import {Car, CarsService} from './cars.service';

@Component({
  selector: 'app-cars',
  template: `{{maxSpeedWithoutCheck}}`,
  styles: []
})
export class Car1Component implements OnInit {
  private car;

  constructor(private carsService: CarsService) {
  }

  getCar(id) {
    this.carsService.getCarWithoutModel(id).subscribe((car) => {
      this.car = car;
    });
  }

  get maxSpeedWithoutCheck() {
    return this.car.maxSpeed;
  }

  get maxSpeedWithCheck() {
    return this.car ? this.car.maxSpeed : '';
  }

  ngOnInit(): void {
    this.getCar(1);
    console.log(this.maxSpeedWithoutCheck);
  }
}
