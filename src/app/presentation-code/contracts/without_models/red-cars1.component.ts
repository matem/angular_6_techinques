import {Component, OnInit} from '@angular/core';
import {Car, CarsService} from './cars.service';

@Component({
  selector: 'app-red-cars-1',
  template: ``,
  styles: []
})
export class RedCars1Component implements OnInit {
  private redCars;

  constructor(private carsService: CarsService) {
  }

  ngOnInit(): void {
    this.getRedCars();
  }

  getRedCars() {
    this.carsService.getCarsWithoutModel().subscribe((response) => {
      if (response['cars'] && Array.isArray(response['cars'])) {
        let redCars = <Array<any>>response['cars'];
        redCars = redCars.map((carObject) => {
          return new Car(carObject);
        }).filter((car) => {
          return car.isRed();
        });
        this.redCars = redCars;
      } else {
        this.redCars = [];
      }
    });
  }
}
