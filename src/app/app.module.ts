import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {Car2Component} from './presentation-code/contracts/with_models/car2.component';
import {Car1Component} from './presentation-code/contracts/without_models/car1.component';
import {RedCars1Component} from './presentation-code/contracts/without_models/red-cars1.component';
import {RedCars2Component} from './presentation-code/contracts/with_models/red-cars2.component';
import {Cars2Component} from './presentation-code/contracts/with_models/cars2.component';
import {Cars1Component} from './presentation-code/contracts/without_models/cars1.component';

@NgModule({
  declarations: [
    AppComponent,
    Car1Component,
    Car2Component,
    RedCars1Component,
    RedCars2Component,
    Cars1Component,
    Cars2Component,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
